<h1>Minimal Music Player (Re-Uploaded)</h1>

<p>The "Minimal Music Player" project is a free access code (uploaded to GitLab by the user for backup purposes) for HTML and PHP website constructions.</p>

<br />

<h2>WARNING!</h2>
<p>1.- This code is freely accessible, and so are the files published within this store.</p>
<p>2.- Reuse is allowed as long as credits are given to the code creators (available in the "credits" section).</p>

<br />

<h2>CREDITS</h2>
<p>Minimal Music Player by <a href="https://dribbble.com/shots/4240318-Made-with-InVision-Studio-Music-Player" target="_blank">Charles Patterson.</a></p>
<p>Minimal Music Player Pen by <a href="https://codepen.io/daniel-c-rdenas-luengo/pen/XPWPZN" target="_blank">Daniel Cárdenas Luengo.</a></p>

<br >

<p>NEVER STOP PROGRAMMING.</p>
<p>Luciano Sánchez</p>

<img src="https://loverandom.cl/wp-content/uploads/2018/12/logo-lrcl-dark.png">